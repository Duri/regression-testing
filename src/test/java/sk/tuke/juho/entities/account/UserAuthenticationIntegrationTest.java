package sk.tuke.juho.entities.account;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;

import sk.tuke.juho.config.WebSecurityConfigurationAware;

public class UserAuthenticationIntegrationTest extends WebSecurityConfigurationAware {

    private static String SEC_CONTEXT_ATTR = HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY;

//    @Test
//    public void requiresAuthentication() throws Exception {
//        mockMvc.perform(get("http://localhost:8080/customLogin.xhtml"))
//                .andExpect(redirectedUrl("/customLogin.xhtml"));
//    }

    @Test
    public void userAuthenticates() throws Exception {
        final String username = "user1@user.com";

        mockMvc.perform(post("http://localhost:8080/appLogin").param("app_username", username).param("app_password", "0000"))
                .andExpect(redirectedUrl("/secure/homeSignedIn.xhtml"))
                .andExpect(r -> Assert.assertEquals(((SecurityContext) r.getRequest().getSession().getAttribute(SEC_CONTEXT_ATTR)).getAuthentication().getName(), username));

    }

    @Test
    public void userAuthenticationFails() throws Exception {
        final String username = "user1@user.com";
        mockMvc.perform(post("/appLogin").param("inputEmail", username).param("inputPassword", "invalid"))
                .andExpect(redirectedUrl("/customLogin.xhtml?error"))
                .andExpect(r -> Assert.assertNull(r.getRequest().getSession().getAttribute(SEC_CONTEXT_ATTR)));
    }
}
