package sk.tuke.juho.controllers.entities.Jenkins;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import sk.tuke.juho.controllers.entities.account.Account;
import sk.tuke.juho.controllers.entities.account.AccountService;
import sk.tuke.juho.controllers.entities.team.Team;
import sk.tuke.juho.utils.Constants;


import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.time.Instant;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Juraj on 10.4.2017.
 */
@RunWith(MockitoJUnitRunner.class)
@FixMethodOrder(value = MethodSorters.NAME_ASCENDING)
public class JenkinsTest {
    private Jenkins jenkins;
    private Account admin;
    private Team team;
    private Account notAdmin = new Account("bad@mail.com", "0000", "unauthorized");

    @Before
    public void bef(){
        admin = new Account("admin@ad.com","0000","ADMIN","Admin Michal");
        notAdmin = new Account("bad@mail.com", "0000", "unauthorized");
        //generate ID instead of DB trigger
        admin.setIdAccount((long) 12345);
        notAdmin.setIdAccount((long)666);

        team = new Team("test team", admin);
        jenkins = new Jenkins(8099);
        team.setJenkins(jenkins);
    }

    @Test
    public void test1wasStartedAndGenerated() throws Exception {
        assertThat(jenkins.wasStartedAndGenerated()).isFalse();

        jenkins.setLastStartTime(Date.from(Instant.now()));
        assertThat(jenkins.wasStartedAndGenerated()).isTrue();
    }

    @Test
    public void test2isAdmin() throws Exception{

        //control if admin is admin and notAdmin does not admin
        assertThat(jenkins.isAdmin(notAdmin)).isFalse();
        assertThat(team.isAdmin(notAdmin)).isFalse();
        assertThat(jenkins.isAdmin(admin)).isTrue();
        assertThat(team.isAdmin(admin)).isTrue();
    }

    @Test
    public void test3adminIsLonalyMemberOfTeam(){
        //control if list of account contains of only Admin
        assertThat(team.getAccounts().size()).isEqualTo(1);
        assertThat(team.getAccounts().contains(admin)).isTrue();
    }

    @Test
    public void test4IfTeamAndJenkinsWasGeneratedInThePast(){
        assertThat(team.getCreated()).isLessThan(Instant.now());
        assertThat(jenkins.getCreated()).isLessThan(Instant.now());
    }

    @Test
    public void test5isPossibleSendMail() throws Exception {
        jenkins.setLastStartTime(Date.from(Instant.now()));

        assertThat(jenkins.isPossibleSendMail()).isTrue();

        //send mail
        jenkins.setLastSecurityEmail(Date.from(Instant.now()));
        assertThat(jenkins.isPossibleSendMail()).isFalse();

        jenkins.setLastSecurityEmail(new Date(System.currentTimeMillis()-Constants.RESEND_EMAIL_TIMEOUT_IN_MILISECS));

        //after timeout it is possible
        assertThat(jenkins.isPossibleSendMail()).isTrue();
    }



}