/**
 * Created by Juraj on 25.6.2016.
 */

// $('#table-proj').editableTableWidget({editor: $('<textarea>')});

var editor; // use a global for the submit and return data rendering in the examples

$(document).ready(function () {
    editor = new $.fn.dataTable.Editor({
        ajax: "/mackopes",
        table: "#example",
        fields: [{
            label: "Project name:",
            name: "projectName"
        }, {
            label: "Link:",
            name: "link"
        }, {
            label: "Team:",
            name: "team"
        }
        ]
    });

    var table = $('#example').DataTable({
        lengthChange: false,
        ajax: "/mackopes2",
        columns: [
            {data: "projectName"},
            {data: "link"},
            {data: "team"}
        ],
        select: true
    });

    // Display the buttons
    new $.fn.dataTable.Buttons(table, [
        {extend: "create", editor: editor},
        {extend: "edit", editor: editor},
        {extend: "remove", editor: editor}
    ]);

    table.buttons().container()
        .appendTo($('.col-sm-6:eq(0)', table.table().container()));
});
