/**
 * Created by Juraj on 25.6.2016.
 */
$(document).ready(function () {
    var headerHeight = $('body>.header').height();
    if (headerHeight == undefined)
        headerHeight = 0;

    var newContainerHeight = $(window).height() -
        headerHeight -
        $('body>.footer').height() - 64;
    if (newContainerHeight > $("body>.container").height())
        $('body>.container').height(newContainerHeight);


    $('.header-section').each(function (index, obj) {
        $('.header-section').removeClass("active");
        if ("/"+obj.id == window.location.pathname) {
            $("#"+obj.id).addClass("active");
        }
    });

    // if($('#example'))
    //     $('#example').DataTable();
});
