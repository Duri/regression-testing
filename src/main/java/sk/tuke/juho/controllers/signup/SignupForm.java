package sk.tuke.juho.controllers.signup;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sk.tuke.juho.controllers.entities.account.Account;
import sk.tuke.juho.controllers.entities.account.AccountService;
import sk.tuke.juho.controllers.entities.team.Team;
import sk.tuke.juho.controllers.entities.team.TeamRepository;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.validation.constraints.Size;

@ManagedBean(name = "signupForm")
@RequestScoped
@Component
public class SignupForm {

    @Autowired
    private AccountService accountService;

    @Autowired
    private TeamRepository teamRepository;

    private static final String NOT_BLANK_MESSAGE = "Blank message !!!";

    @NotBlank(message = SignupForm.NOT_BLANK_MESSAGE)
    @Email(message = "Bad email format!")
    private String email;

    @NotBlank(message = SignupForm.NOT_BLANK_MESSAGE)
    private String password;

    @Size(min=4, max = 16, message = "Name should contain 4-16 characters")
    private String name;

    private String team_uuid;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeam_uuid() {
        return team_uuid;
    }

    public void setTeam_uuid(String team_uuid) {
        this.team_uuid = team_uuid;
    }

    public void reset() {
        setEmail("");
        setName("");
        setPassword("");
    }

    public String createAccount() {
        Account account = accountService.save(new Account(getEmail(), getPassword(), "ROLE_USER", getName()));
        Team team = teamRepository.findOneByUuid(getTeam_uuid());
        if (team != null) {
            team.addAccount(account);
            teamRepository.saveAndFlush(team);
        }
        accountService.signin(account);

        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Registration success", "success");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        return "/";
    }
}
