package sk.tuke.juho.controllers.entities.Jenkins;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by Juraj on 19.6.2016. Objekt reprezentuje jeden z Jenkins CLI príkazov
 */
@SuppressWarnings("serial")
@Entity
public class JenkinsCli implements java.io.Serializable {

    @Id
    private int idJenkinsCli;

    private String command;

    /**
     * constructor
     */
    public JenkinsCli() {
    }

    public int getIdJenkinsCli() {
        return idJenkinsCli;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }
}

