package sk.tuke.juho.controllers.entities.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Servis, ktorý obsahuje operácie s objektami vkladanými do databázy
 */
@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class AccountService implements UserDetailsService, AuthenticationUserDetailsService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     *
     * @param account user account for save
     * @return saved account
     */
    @Transactional
    public Account save(Account account) {
        account.setPassword(passwordEncoder.encode(account.getPassword()));
        accountRepository.save(account);
        return account;
    }

    /**
     *
     * @return list of initialized accounts
     */
    public List<Account> initialize() {
        Account user1 = new Account("user1@user.com", "0000", "ROLE_USER", "Jožko Praclík");
        Account user2 = new Account("user2@user.com", "0000", "ROLE_USER", "Michal Biely");
        Account user3 = new Account("user3@user.com", "0000", "ROLE_USER", "Alojz Vrak");
        Account admin = new Account("Juraj.Horbal@gmail.com", "0000", "ROLE_ADMIN", "Tobiáš Panda");
        save(admin);
        save(user2);
        save(user1);
        save(user3);
        List<Account> accounts = new ArrayList<>();
        accounts.add(user1);
        accounts.add(user2);
        accounts.add(user3);
        accounts.add(admin);
        return accounts;
    }

    /**
     *
     * @param username user name
     * @return object of User Details, that is generated from account
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Account account = accountRepository.findOneByEmail(username);
        if (account == null) {
            throw new UsernameNotFoundException("user not found");
        }
        return createUser(account);
    }

    /**
     *
     * @param account current account
     */
    public void signin(Account account) {
        SecurityContextHolder.getContext().setAuthentication(authenticate(account));
    }


    private Authentication authenticate(Account account) {
        return new UsernamePasswordAuthenticationToken(createUser(account), null, Collections.singleton(createAuthority(account)));
    }

    private User createUser(Account account) {
        return new User(account.getEmail(), account.getPassword(), Collections.singleton(createAuthority(account)));
    }

    private GrantedAuthority createAuthority(Account account) {
        return new SimpleGrantedAuthority(account.getRole());
    }

    /**
     *
     * @param authentication
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserDetails(Authentication authentication) throws UsernameNotFoundException {
        Account account = accountRepository.findOneByEmail(authentication.getName());
        if (account == null) {
            throw new UsernameNotFoundException("user not found");
        }
        return createUser(account);
    }
}
