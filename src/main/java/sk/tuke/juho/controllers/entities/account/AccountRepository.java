package sk.tuke.juho.controllers.entities.account;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * prístup pre vytváranie databázových dotazov pre Account objekty
 */
@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
    /**
     *
     * @param email
     * @return users account by email
     */
    Account findOneByEmail(String email);
}