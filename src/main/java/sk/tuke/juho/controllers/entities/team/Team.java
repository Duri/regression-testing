package sk.tuke.juho.controllers.entities.team;

import com.fasterxml.jackson.annotation.JsonView;
import sk.tuke.juho.controllers.entities.Jenkins.Jenkins;
import sk.tuke.juho.controllers.entities.account.Account;
import sk.tuke.juho.controllers.entities.project.Project;
import sk.tuke.juho.view.Views;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * Created by Juraj Horbaľ on 19.6.2016. Objekt reprezentujúci tím
 */
@SuppressWarnings("serial")
@Entity
public class Team implements java.io.Serializable {

    @Id
    @GeneratedValue
    private Long idTeam;

    /**
     * uuid for new members
     */
    private String uuid;

    private String teamName;

    @JsonView(Views.Public.class)
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Jenkins jenkins;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "team_idteam")
    private List<Project> projects = new ArrayList<>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "idAccount")
    private List<Account> accounts;

    private long admin;

    private Instant created;

    /**
     * constructor
     */
    public Team() {
        this.uuid = UUID.randomUUID().toString();
        this.created = Instant.now();
        this.accounts = new ArrayList<>();
    }

    /**
     * constructor
     * @param teamName team name
     * @param admin user, that is administrator of team
     * @param accounts list of members
     */
    public Team(String teamName, Account admin, Account... accounts) {
        this();
        this.accounts.addAll(Arrays.asList(accounts));
        this.accounts.add(admin);
        this.teamName = teamName;
        this.admin = admin.getIdAccount();
    }

    /**
     *
     * @param user controlled user
     * @return true if user is administrator of current team
     */
    public boolean isAdmin(Account user){
        return admin == user.getIdAccount();
    }

    @Override
    public String toString() {
        return this.teamName;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public Jenkins getJenkins() {
        return jenkins;
    }

    /**
     * Set jenkins instance and set path for jenkins by team name
     * @param jenkins instance of Jenksin
     */
    public void setJenkins(Jenkins jenkins) {
        jenkins.setPath(teamName.toLowerCase().replaceAll("[^a-zA-Z\\d:]", "_"));
        jenkins.setAdmin(admin);
        this.jenkins = jenkins;
    }

    public void addAccount(Account a) {
        accounts.add(a);
    }

    public boolean teamContainsUser(Account user) {
        return accounts.contains(user);
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public Long getIdTeam() {
        return idTeam;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public long getAdmin() {
        return admin;
    }

    public void setAdmin(long admin) {
        this.admin = admin;
    }

    public Instant getCreated() {
        return created;
    }

    public String getUuid() {
        return uuid;
    }
}
