package sk.tuke.juho.controllers.entities.account;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.Instant;

/**
 * Reprezentuje používateľský účet
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "account")
public class Account implements java.io.Serializable {

    @Id
    @GeneratedValue
    private Long idAccount;

    @Column(unique = true)
    private String email;

    @JsonIgnore
    private String password;

    private String name;

    private String role = "ROLE_USER";

    private Instant created;


    protected Account() {
    }

    /**
     * constructor
     * @param email
     * @param password
     * @param role
     */
    public Account(String email, String password, String role) {
        this.email = email;
        this.password = password;
        this.role = role;
        this.created = Instant.now();
    }

    /**
     * constructor
     * @param email
     * @param password
     * @param role
     * @param name
     */
    public Account(String email, String password, String role, String name) {
        this(email, password, role);
        this.name = name;
    }

    public Long getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(Long idAccount) {
        this.idAccount = idAccount;
    }

    public void setCreated(Instant created) {
        this.created = created;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Instant getCreated() {
        return created;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean hasRole(String role) {
        return this.role.equals(role);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        if (idAccount != null ? !idAccount.equals(account.idAccount) : account.idAccount != null) return false;
        if (email != null ? !email.equals(account.email) : account.email != null) return false;
        if (password != null ? !password.equals(account.password) : account.password != null) return false;
        if (name != null ? !name.equals(account.name) : account.name != null) return false;
        if (role != null ? !role.equals(account.role) : account.role != null) return false;
        return created != null ? created.equals(account.created) : account.created == null;

    }

    @Override
    public int hashCode() {
        int result = idAccount != null ? idAccount.hashCode() : 0;
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (role != null ? role.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        return result;
    }
}
