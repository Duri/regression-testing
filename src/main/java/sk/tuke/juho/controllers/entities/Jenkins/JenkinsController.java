package sk.tuke.juho.controllers.entities.Jenkins;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;

/**
 * Kontroler pre objekty typu Jenkins
 */

@Controller
class JenkinsController {

    private JenkinsRepository jenkinsRepository;

    @Autowired
    public JenkinsController(JenkinsRepository projectRepository) {
        this.jenkinsRepository = projectRepository;
    }

    @RequestMapping(value = "jenkins/current", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public Jenkins currentAccount(Principal principal) {
        Assert.notNull(principal);
        return jenkinsRepository.findOne((long) 0);
    }

    @RequestMapping(value = "jenkins/{path}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public void account(@PathVariable("path") String path, HttpServletResponse response) {
        Jenkins j = jenkinsRepository.findOneByPath(path);
        try {
            response.sendRedirect(j.getUrlForJenkins());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
//    @RequestMapping(value = "jenkins/{id}", method = RequestMethod.GET)
//    @ResponseStatus(value = HttpStatus.OK)
//    @ResponseBody
//    @Secured("ROLE_ADMIN")
//    public Jenkins account(@PathVariable("id") Long id) {
//        return jenkinsRepository.findOne(id);
//    }
}
