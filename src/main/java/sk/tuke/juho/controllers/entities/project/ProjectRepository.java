package sk.tuke.juho.controllers.entities.project;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sk.tuke.juho.controllers.entities.team.Team;

import java.util.List;

/**
 * Databázové operácie s Project objektami
 */
@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {
    List<Project> findByTeamIn(List<Team> teams);
}