package sk.tuke.juho.controllers.entities.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import sk.tuke.juho.controllers.entities.Jenkins.JenkinsRepository;
import sk.tuke.juho.controllers.entities.account.AccountRepository;

@Controller
class TeamController {

    private TeamRepository teamRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private JenkinsRepository jenkinsRepository;

    @Autowired
    public TeamController(TeamRepository accountRepository) {
        this.teamRepository = accountRepository;
    }


//    @RequestMapping(value = "team", method = RequestMethod.GET)
//    @ResponseStatus(value = HttpStatus.OK)
//    @ResponseBody
//    @Secured({"ROLE_USER", "ROLE_ADMIN"})
//    public Team currentAccount(Principal principal) {
//        Assert.notNull(principal);
//        return teamRepository.findOne((long) 0);
//    }
//
//    @RequestMapping(value = "team/{id}", method = RequestMethod.GET)
//    @ResponseStatus(value = HttpStatus.OK)
//    @ResponseBody
//    @Secured("ROLE_ADMIN")
//    public Team account(@PathVariable("id") Long id) {
//        return teamRepository.findOne(id);
//    }


//    @RequestMapping(value = "team/startJenkins/{id}", method = RequestMethod.GET)
//    @ResponseStatus(value = HttpStatus.OK)
//    public boolean startJenkins(Principal principal, HttpServletRequest request, @PathVariable("id") Long id) {
//        Team team = teamRepository.findOne(id);
//        Account user = accountRepository.findOneByEmail(principal.getName());
//
//        if(team.teamContainsUser(user)) {
//            String absolutePath = Constants.getAbsolutePath();
//
//            Jenkins jenkins = team.getJenkins();
//
//            String javaHome = absolutePath+"\\jenkinses\\Jenkins\\jre\\bin";
//            String jenkinsWarHome = absolutePath+"\\jenkinses\\usr";
//            String pathToDirBat = absolutePath+"\\Jenkinses\\" + team.getJenkins().getPath();//C:\Users\Administrator
//
//            Map<String, String> params = new HashMap<>();
//            params.put("jeknins_apj", String.valueOf(jenkins.getApjPort()));
//            params.put("jenkins_port_j", String.valueOf(jenkins.getIdJenkins()));
//            params.put("jenkins_prefix", team.getJenkins().getPath());
//            params.put("jenkins_war_home", jenkinsWarHome);
//            params.put("java_home_j", javaHome);
//            params.put("jenkins_home_j", pathToDirBat);
//            params.put("user", principal.getName());
//            params.put("passw", "heslo");
//            JenkinsServer jenkinsServer;
//            try {
//                jenkinsServer = JenkinsGen.getInstance("8079","", "duri","14721993").getJenkinsServer();
//                JobWithDetails jwd = jenkinsServer.getJob("jenko");
////                jwd.getUrl().replace("htto://localhost:8080")
//                jwd.build(params);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            JenkinsServer newJenk = JenkinsGen.getInstance(String.valueOf(jenkins.getIdJenkins()), "/" + team.getJenkins().getPath()).getJenkinsServer();
//            int i=0;
//            while (!newJenk.isRunning()) {
//                try {
//                    if(i>50)
//                        return false;
//                    i++;
//                    Thread.sleep(250);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//            boolean isFirstStart = jenkins.getLastStartTime()== null;
//            jenkins.setLastStartTime(new Date(System.currentTimeMillis()));
//            if(isFirstStart) {
//                String secretKey = getSecretKey(team.getJenkins().getPath());
//                EmailSender.sendMail(user.getEmail(), "Kľúč do Jenkins",
//                        "Ahoj, " + user.getName() + ",\n\n" +
//                                "nasledujúci kľúč \n" +
//                                secretKey +
//                                " potrebuješ použiť na linke:" +
//                                team.getJenkins().getUrlForJenkins());
//                jenkins.setLastSecurityEmail(new Date(System.currentTimeMillis()));
//            }
//            jenkinsRepository.save(jenkins);
//            return true;//"Running";
//        }
//        return false;//"It is not your Jenkins!!!";
//    }
//
//    @RequestMapping(value = "team/jenkinsSecret/{id}", method = RequestMethod.GET)
//    @ResponseStatus(value = HttpStatus.OK)
//    public boolean sendJenkinsSecret(Principal principal, HttpServletRequest request, @PathVariable("id") Long id) {
//        Team team = teamRepository.findOne(id);
//        Account user = accountRepository.findOneByEmail(principal.getName());
//
//        if(team.teamContainsUser(user)) {
//            String secretKey = getSecretKey(team.getJenkins().getPath());
//            EmailSender.sendMail(user.getEmail(),"Kľúč do Jenkins",
//                    "Ahoj, "+user.getName()+",\n\n" +
//                            "nasledujúci kľúč \n"+
//                            secretKey +
//                            " potrebuješ použiť na linke:" +
//                            team.getJenkins().getUrlForJenkins());
//            return true;//"Running";
//        }
//        return false;//"It is not your Jenkins!!!";
//    }
//
//    private String getSecretKey(String path) {
//        String secretFile = Constants.getAbsolutePath()+"/jenkinses/"+path+"/secrets/initialAdminPassword";
//        try {
//            return readFile(secretFile);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return "notCredentials";
//    }
//
//    private String readFile(String fileName) throws IOException {
//        BufferedReader br = new BufferedReader(new FileReader(fileName));
//        try {
//            StringBuilder sb = new StringBuilder();
//            String line = br.readLine();
//
//            while (line != null) {
//                sb.append(line);
//                sb.append("\n");
//                line = br.readLine();
//            }
//            return sb.toString();
//        } finally {
//            br.close();
//        }
//    }

}
