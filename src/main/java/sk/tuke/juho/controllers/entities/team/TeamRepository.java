package sk.tuke.juho.controllers.entities.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sk.tuke.juho.controllers.entities.account.Account;

import java.util.List;

/**
 * Operácie pre obejkty Team
 */
@Repository
public interface TeamRepository extends JpaRepository<Team, Long> {
    @Query("SELECT t from Team t inner join t.accounts a where a = :account")
    List<Team> findByAccounts(@Param("account") Account account);

    Team findOneByTeamName(String team);

    Team findOneByUuid(String uuid);
}