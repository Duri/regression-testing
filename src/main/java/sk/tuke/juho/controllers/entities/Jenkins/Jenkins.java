package sk.tuke.juho.controllers.entities.Jenkins;

import com.fasterxml.jackson.annotation.JsonView;
import sk.tuke.juho.controllers.entities.account.Account;
import sk.tuke.juho.utils.Constants;
import sk.tuke.juho.view.Views;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.Instant;
import java.util.Date;

/**
 * Created by Juraj Horbaľ on 19.6.2016. Objekt reprezentuje Jenkins inštanciu
 */
@SuppressWarnings("serial")
@Entity
public class Jenkins implements java.io.Serializable {

    @Id
    private int idJenkins;

    @JsonView(Views.Public.class)
    @Column(unique = true)
    private int shutdownPort;

    @JsonView(Views.Public.class)
    @Column(unique = true)
    private int redirectPort;

    @JsonView(Views.Public.class)
    @Column(unique = true)
    private int apjPort;

    @JsonView(Views.Public.class)
    @Column(unique = true)
    private String path;

    private Date lastSecurityEmail;
    private Date lastStartTime;

    private long admin;

    private Instant created;

    private String status;

    private String statusClasses;

    /**
     * constructor
     */
    public Jenkins() {
        this.created = Instant.now();
        status = "Štart";
        statusClasses = "btn btn-success";
    }

    /**
     * constructor
     *
     * @param port main port of Jenkins instance
     */
    public Jenkins(int port) {
        this(port, port - 75, port + 363, port - 71);
    }

    private Jenkins(int port, int shutdownPort, int redirectPort, int apjPort) {
        this();
        this.idJenkins = port;              //8080
        this.shutdownPort = shutdownPort;   //8005
        this.redirectPort = redirectPort;   //8443
        this.apjPort = apjPort;             //8009
    }


    public boolean isRuning() {
        return Constants.isPageOnline(getUrlForJenkins());
    }

    public int getIdJenkins() {
        return idJenkins;
    }

    public void setIdJenkins(int idJenkins) {
        this.idJenkins = idJenkins;
    }

    public int getShutdownPort() {
        return shutdownPort;
    }

    public int getRedirectPort() {
        return redirectPort;
    }

    public int getApjPort() {
        return apjPort;
    }

    public String getApjPortS() {
        return String.valueOf(apjPort);
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    /**
     * @return url for Jenkins on the host next to tris application
     */
    public String getUrlForJenkins() {
        return "http://" + Constants.getLocalHostIP() + ":" + idJenkins + "/" + this.path;
    }

    public Date getDateOfLastSecurityEmail() {
        return lastSecurityEmail;
    }

    public void setLastSecurityEmail(Date lastSecurityEmail) {
        this.lastSecurityEmail = lastSecurityEmail;
    }

    public Date getLastStartTime() {
        return lastStartTime;
    }

    public void setLastStartTime(Date lastStartTime) {
        this.lastStartTime = lastStartTime;
    }

    public boolean wasStartedAndGenerated() {
        return lastStartTime != null;
    }

    public long getAdmin() {
        return admin;
    }

    public void setAdmin(long admin) {
        this.admin = admin;
    }

    public boolean isAdmin(Account user) {
        return admin == user.getIdAccount();
    }

    /**
     * Control if it is possible to send another email. It is possible each 30 minutes (see constants)
     *
     * @return
     */
    public boolean isPossibleSendMail() {
        if (lastStartTime == null) {
            System.err.println("Jenkins is not generated yet! Please power on it first.");
            return false;
        }
        long lastEmailInMs = lastSecurityEmail == null ? 0 : lastSecurityEmail.getTime();
        if (lastEmailInMs >
                (System.currentTimeMillis() - Constants.RESEND_EMAIL_TIMEOUT_IN_MILISECS)) {
            System.out.println("You have to wait til " + new Date(lastSecurityEmail.getTime() + Constants.RESEND_EMAIL_TIMEOUT_IN_MILISECS));
            return false;
        }
        return true;
    }

    public Instant getCreated() {
        return created;
    }

    public void setCreated(Instant created) {
        this.created = created;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusClasses() {
        return statusClasses;
    }

    public void setStatusClasses(String statusClasses) {
        this.statusClasses = statusClasses;
    }

    public String getJenkinsPort() {
        return String.valueOf(idJenkins);
    }
}

