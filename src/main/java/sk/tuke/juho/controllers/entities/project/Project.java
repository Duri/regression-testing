package sk.tuke.juho.controllers.entities.project;

import com.fasterxml.jackson.annotation.JsonView;
import sk.tuke.juho.controllers.entities.team.Team;
import sk.tuke.juho.view.Views;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Juraj Horbaľ on 19.6.2016. Reprezentuje objekt projektu
 */
@SuppressWarnings("serial")
@Entity
public class Project implements java.io.Serializable {

    @Id
    @GeneratedValue
    private Long idProject;

    @JsonView(Views.Public.class)
    private String projectName;

    @JsonView(Views.Public.class)
    private String link;

//    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    @JoinColumn(name = "project_idproject")
//    private List<String> codelines = new ArrayList<>();
    private String codelines;

    @JsonView(Views.Public.class)
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Team team;

    private Instant created;

    /**
     * constructor
     */
    public Project() {
    }

    /**
     * constructor
     * @param projectName project name
     * @param link url to project on repository
     * @param team team instance
     */
    public Project(String projectName, String link, Team team) {
        this.created = Instant.now();
        this.team = team;
        this.projectName = projectName;
        this.link = link;
        team.getProjects().add(this);
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Team getTeam() {
        return team;
    }

    public Long getIdProject() {
        return idProject;
    }

    public void setIdProject(Long idProject) {
        this.idProject = idProject;
    }

    public Project(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getCodelines() {
        return codelines;
    }

    public void setCodelines(String codelines) {
        this.codelines = codelines;
    }

    public Instant getCreated() {
        return created;
    }

    public void setCreated(Instant created) {
        this.created = created;
    }
}

