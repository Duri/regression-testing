package sk.tuke.juho.controllers.entities;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import sk.tuke.juho.controllers.entities.Jenkins.Jenkins;
import sk.tuke.juho.controllers.entities.account.Account;
import sk.tuke.juho.controllers.entities.account.AccountService;
import sk.tuke.juho.controllers.entities.project.Project;
import sk.tuke.juho.controllers.entities.project.ProjectRepository;
import sk.tuke.juho.controllers.entities.team.Team;
import sk.tuke.juho.controllers.entities.team.TeamRepository;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Created by Juraj on 24.6.2016.
 */
@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UniversalService {

    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private AccountService accountService;

    /**
     * initialize database
     */
    @PostConstruct
    public void initialize() {
        if (projectRepository == null || projectRepository.count() == 0) {
            List<Account> accounts = accountService.initialize();

            Team team1 = new Team("Team 1", accounts.get(0), accounts.get(1));
            Team team2 = new Team("Team 2", accounts.get(1), accounts.get(2));
            Team team3 = new Team("Head", accounts.get(3));
            Jenkins jenkins = new Jenkins(8079);
            team3.setJenkins(jenkins);
            Jenkins jenkins1 = new Jenkins(8081);
            team1.setJenkins(jenkins1);
            Jenkins jenkins2 = new Jenkins(8082);
            team2.setJenkins(jenkins2);


            Project project1 = new Project("Project 1", "https://github.com/jenkinsci/jenkins.git", team1);
            Project project2 = new Project("Project 2", "https://github.com/apache/tomcat", team2);
            Project project3 = new Project("Head", "https://bitbucket.org/Duri/regression-testing", team3);
            teamRepository.save(team3);
            teamRepository.save(team1);
            teamRepository.save(team2);
            projectRepository.save(project1);
            projectRepository.save(project2);
            projectRepository.save(project3);
        }
    }
}
