package sk.tuke.juho.controllers.entities.Jenkins;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Správa databázových objektov Jenkins CLI
 */
@Repository
public interface JenkinsCliRepository extends JpaRepository<JenkinsCli, Long> {


    @Query("SELECT j from JenkinsCli j")
    List<JenkinsCli> getCommands();
}