package sk.tuke.juho.controllers.entities.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;

/**
 * Kontroler pre správu Account objektov
 */
@Controller
@ManagedBean(name = "account", eager = true)
@ViewScoped
class AccountController implements Serializable {

    private AccountRepository accountRepository;

    @Autowired
    public AccountController(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    /**
     *
     * @param principal
     * @return actual user
     */
    @RequestMapping(value = "account/current", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    @Secured({"ROLE_USER"})
    public Account currentAccount(Principal principal) {
        Assert.notNull(principal);
        return accountRepository.findOneByEmail(principal.getName());
    }

    /**
     * Show account properties for administrator od application
     * @param id of account
     * @return account by id
     */
    @RequestMapping(value = "account/{id}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    @Secured("ROLE_ADMIN")
    public Account account(@PathVariable("id") Long id) {
        return accountRepository.findOne(id);
    }

    /**
     *
     * @param principal
     * @param request
     * @return login page
     */
    @RequestMapping(value = "login", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public ModelAndView login(Principal principal, HttpServletRequest request) {
        accountRepository.findOneByEmail(principal.getName());
        URI uri = null;
        try {
            uri = new URI(request.getParameter("service"));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return new ModelAndView("redirect:" + uri.getAuthority() + uri.getPath().substring(0, uri.getPath().indexOf("/", 2)));
    }
}
