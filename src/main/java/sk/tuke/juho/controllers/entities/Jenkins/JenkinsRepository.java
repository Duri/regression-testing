package sk.tuke.juho.controllers.entities.Jenkins;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Databázové operácie pre ukladanie a získavanie Jenkins objektov z databázy
 */

@Repository
public interface JenkinsRepository extends JpaRepository<Jenkins, Long> {

    Jenkins findOneByPath(String path);

    @Query("SELECT max(j.idJenkins) from Jenkins j")
    int getMaxId();

}