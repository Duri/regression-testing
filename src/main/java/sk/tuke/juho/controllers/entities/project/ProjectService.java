package sk.tuke.juho.controllers.entities.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Operácie s Projektami
 */

@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ProjectService {

    @Autowired
    private ProjectRepository projectService;

    @Transactional
    public Project save(Project project) {
        projectService.save(project);
        return project;
    }
}
