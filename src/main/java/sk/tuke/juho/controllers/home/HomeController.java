package sk.tuke.juho.controllers.home;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import sk.tuke.juho.controllers.entities.account.Account;
import sk.tuke.juho.controllers.entities.account.AccountRepository;

import java.security.Principal;

@Controller
public class HomeController {

    @Autowired
    private AccountRepository accountRepository;

    @RequestMapping(value = {"/"},
            method = RequestMethod.GET)
    public String index(Principal principal, Model model) {
        if (principal == null)
            return "customLogin.xhtml";
        Account user = accountRepository.findOneByEmail(principal.getName());
        if (user.getRole().equals("ROLE_ADMIN"))
            return "secure/admin.xhtml";
        return "secure/homeSignedIn.xhtml";
    }


    @RequestMapping(value = "/about")
    public String aaa() {
        return "templates/layout.xhtml";
    }

    @RequestMapping(value = "/reg")
    public String register() {
        return "/signup";
    }

    //    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/users")
    public String users() {
        return "secure/admin.xhtml";
    }
}
