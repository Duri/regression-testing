package sk.tuke.juho.view;

        import com.fasterxml.jackson.annotation.JsonView;

        import java.util.List;

/**
 * Created by Juraj on 26.6.2016.
 */
public class AjaxResponseBody {

    @JsonView(Views.Public.class)
    List<ObjectView> data;

    @JsonView(Views.Public.class)
    String options;

    @JsonView(Views.Public.class)
    String file;

    public List<ObjectView> getData() {
        return data;
    }

    public void setData(List<ObjectView> data) {
        this.data = data;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
}
