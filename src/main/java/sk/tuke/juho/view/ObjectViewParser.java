package sk.tuke.juho.view;

import sk.tuke.juho.controllers.entities.project.Project;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Juraj on 26.6.2016.
 */
public class ObjectViewParser {

    public static List<ObjectView> parse(List<Project> objects) {
        List<ObjectView> projects = new ArrayList<>();
        for (Project p : objects) {
            ProjectView projectView = new ProjectView();
            projectView.setLink(p.getLink());
            projectView.setProjectName(p.getProjectName());
            projectView.setTeam(p.getTeam().getTeamName());
            projects.add(projectView);
        }
        return projects;
    }
}
