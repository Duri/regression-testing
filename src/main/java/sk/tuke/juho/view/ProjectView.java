package sk.tuke.juho.view;

import com.fasterxml.jackson.annotation.JsonView;

/**
 * Created by Juraj on 26.6.2016.
 */
class ProjectView implements ObjectView {

    @JsonView(Views.Public.class)
    private String projectName;

    @JsonView(Views.Public.class)
    private String link;

    @JsonView(Views.Public.class)
    private String team;

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }
}
