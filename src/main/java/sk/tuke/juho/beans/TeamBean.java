package sk.tuke.juho.beans;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import sk.tuke.juho.controllers.entities.Jenkins.Jenkins;
import sk.tuke.juho.controllers.entities.account.Account;
import sk.tuke.juho.controllers.entities.team.Team;
import sk.tuke.juho.utils.EmailSender;

import javax.faces.bean.ManagedBean;
import java.io.Serializable;

/**
 * Created by Juraj Horbaľ on 27.12.2016. Poskytuje podporu pre spravovanie tímov
 */


@Component
@ManagedBean(name = "teamBean")
//@RequestScoped
@Scope("request")
//@ViewScoped
public class TeamBean extends BeansFather implements Serializable {

    private String teamName;
    private String email;
    private String uuid;


    /**
     * Create new team. Actual user is admin of created Team since now.
     */
    public void createTeam() {
        Account teamAdmin = accountRepository.findOneByEmail(getUserEmail());
        Team newTeam = new Team(teamName,teamAdmin);
        newTeam.setJenkins(new Jenkins(jenkinsRepository.getMaxId() + 1));
        teamRepository.save(newTeam);
    }

    /**
     * Send email for new member of team.
     * @param uuidTeam UUID of team
     * @param teamName name of team
     */
    public void sendEmailForTeamMember(String uuidTeam, String teamName) {
        EmailSender.sendMail(email, "Pozvánka do tímu " + teamName, "Dobrý deň,\n\n" +
                "Boli ste pozvaný do tímu " + teamName + ".\n\n" +
                "Nasledujúce ID prosím použite pre pridanie do nového tímu:\n" +
                "" + uuidTeam);
    }

    /**
     *
     * @return true if actual user is added in to the team which uuid use.
     */
    public boolean addTeamMember() {
        if (uuid == null || uuid.length() < 10)
            return false;
        Team team = teamRepository.findOneByUuid(uuid);
        Account account = accountRepository.findOneByEmail(getUserEmail());
        if (team.teamContainsUser(account))
            return false;
        team.addAccount(account);
        teamRepository.save(team);
        return true;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }
}
