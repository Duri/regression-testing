package sk.tuke.juho.beans;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;


/**
 * Created by Juraj Horbaľ on 2/28/2017. Poskytuje podporu pre jazyky
 */

@ManagedBean(name="language")
@SessionScoped
public class LanguageBean implements Serializable{

    private static final long serialVersionUID = 1L;

    private String localeCode;

    private static Map<String,Object> countries;
    static{
        countries = new LinkedHashMap<String,Object>();
        countries.put("English", Locale.ENGLISH); //label, value
        countries.put("Slovenský",new Locale("sk_SK","SVK"));
    }

    /**
     *
     * @return map of localizations
     */
    public Map<String, Object> getCountriesInMap() {
        return countries;
    }

    /**
     *
     * @return current locale code
     */
    public String getLocaleCode() {
        if(localeCode==null)
            localeCode="Slovenský";
        return localeCode;
    }

    /**
     *
     * @param localeCode current locale code
     */
    public void setLocaleCode(String localeCode) {
        this.localeCode = localeCode;
    }

    /**
     * value change event listener
     * @param e event
     */
    public void countryLocaleCodeChanged(ValueChangeEvent e){

        String newLocaleValue = e.getNewValue().toString();

        //loop country map to compare the locale code
        for (Map.Entry<String, Object> entry : countries.entrySet()) {

            if(entry.getValue().toString().equals(newLocaleValue)){

                FacesContext.getCurrentInstance()
                        .getViewRoot().setLocale((Locale)entry.getValue());

            }
        }
    }

}