package sk.tuke.juho.beans;

import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.model.JobWithDetails;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import sk.tuke.juho.controllers.entities.Jenkins.Jenkins;
import sk.tuke.juho.controllers.entities.account.Account;
import sk.tuke.juho.controllers.entities.team.Team;
import sk.tuke.juho.utils.Constants;
import sk.tuke.juho.utils.EmailSender;
import sk.tuke.juho.utils.JenkinsGen;

import javax.faces.bean.ManagedBean;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Juraj Horbaľ on 27.12.2016. Poskytuje podporu pre operácie s Jenins inštanciami
 */

@Component
@ManagedBean(name = "jenkinsBean")
//@RequestScoped
@Scope("session")
//@ViewScoped
public class JenkinsBean extends BeansFather implements Serializable {

    /**
     * constructor
     */
    public JenkinsBean() {
    }

    /**
     * Get current Status of Jenkins instance - it is shows to end user (aktuálny status Jenkins inštancie určený pre koncového používateľa)
     * @param id of Team (id tímu)
     * @return String that contains of describe of Jenkains state (reťazec, ktorý obsahuje popis stavu Jenkins)
     */
    public String getJenkinsStatus(Long id) {
        Team team = teamRepository.findOne(id);
        return team.getJenkins().getStatus();
    }

    /**
     *
     * @param id of Team (id tímu)
     * @return String, that contains of classes for div element (reťazec, ktorý obsahuje triedy vykresľovaných html elementov)
     */
    public String getJenkinsStatusClasses(Long id) {
        Team team = teamRepository.findOne(id);
        if (getActualId() == team.getAdmin())
            return team.getJenkins().getStatusClasses();
        return "disabled";
    }

    /**
     * Turn off and power on Jenkins instance by id of team. This operation can provide only administrator of selected team.
     * (vypne a zapne Jenkins inčtanciu podľa id tímu. uto operáciu môže vykonať iba administrátor vybraného tímu)
     * @param id of Team (id tímu)
     * @return true if restart was successful (true ak sa podaril reštart)
     */
    public boolean restartJenkins(Long id) {
        Team team = teamRepository.findOne(id);
        Account user = accountRepository.findOneByEmail(getUserEmail());

        if(team.isAdmin(user)){
            shutDown(id);
            return startJenkins(id);
        }
        return false;
    }

    public boolean shutDown(Long id){
        Team team = teamRepository.findOne(id);
        Account user = accountRepository.findOneByEmail(getUserEmail());

        if(team.isAdmin(user)){
            JobWithDetails jwd = getMasterJobByName("shutdown_jenkins");
            if(jwd == null)
                return false;

            String absolutePath = Constants.getAbsolutePath();
            String javaHome = absolutePath + "\\jenkinses\\Jenkins\\jre\\bin";

            Map<String, String> params = new HashMap<>();
            params.put("java_home_j", javaHome);
            params.put("jenkins_cli_home", absolutePath);
            params.put("jenkins_url_with_port_j", team.getJenkins().getUrlForJenkins());
            params.put("user", Constants.jenkinsUserName);
            params.put("passw", Constants.jenkinsUserPassowrd);

            try {
                jwd.build(params);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }
        return false;
    }

    /**
     * Start teams Jenkins instance. User have to be an administrator of team. When it is the first start of instance, it will be send mail with initial information.
     * (Naštartovanie tímovej Jenkins inštancie. Používatel musí byť administrátorom. Ak je to prvé spustenie, administrátor dostane iniciálizačný email.)
     * @param id of Team (id tímu)
     * @return true if start Jenkins was succesfull (true ak spustenie prebehlo vporiadku)
     */
    public boolean startJenkins(Long id) {
        Team team = teamRepository.findOne(id);
        Account user = accountRepository.findOneByEmail(getUserEmail());

        if (team.isAdmin(user)) {
            JobWithDetails jwd = getMasterJobByName("start_Jenkins");
            if(jwd == null)
                return false;

            String absolutePath = Constants.getAbsolutePath();

            Jenkins jenkins = team.getJenkins();
            changeJenkinsStatus(jenkins, "starting", "disabled");
            String javaHome = absolutePath + "\\jenkinses\\Jenkins\\jre\\bin";
            String jenkinsWarHome = absolutePath + "\\jenkinses\\usr";
            String pathToDirBat = absolutePath + "\\jenkinses\\" + jenkins.getPath();//C:\Users\Administrator
            String jenkinsDefaultFolder = absolutePath + "\\jenkinses";

            Map<String, String> params = new HashMap<>();
            params.put("jeknins_apj", jenkins.getApjPortS());
            params.put("jenkins_port_j", jenkins.getJenkinsPort());
            params.put("jenkins_prefix", jenkins.getPath());
            params.put("jenkins_war_home", jenkinsWarHome);
            params.put("java_home_j", javaHome);
            params.put("jenkins_home_j", pathToDirBat);
            params.put("jenkins_default_folder", jenkinsDefaultFolder);

            try {
                jwd.build(params);
            } catch (IOException e) {
                System.err.println("Job start_Jenkins nebol vykonany!!!");
                e.printStackTrace();
            }
            changeJenkinsStatus(jenkins, "start", "btn btn-success");
            if(isJenkinsOnline(jenkins)) {
                boolean isFirstStart = jenkins.wasStartedAndGenerated();
                jenkins.setLastStartTime(new Date(System.currentTimeMillis()));
                if (isFirstStart) {
                    sendJenkinsSecret(id);
                }
                jenkinsRepository.save(jenkins);
                return true;//"Running";
            }
        }
        return false;//"It is not your Jenkins!!!";
    }

    /**
     * if alows user to send init Email many times.
     * @param id of Team (id tímu)
     * @return true if send mail was succesfull (true ak bol email zaslaný úspešne)
     */
    public boolean sendJenkinsSecret(Long id) {
        String email = getUserEmail();
        Team team = teamRepository.findOne(id);
        Account user = accountRepository.findOneByEmail(email);

        if (team.teamContainsUser(user)) {
            Jenkins jenkins = team.getJenkins();
            String secretKey = getSecretKey(jenkins.getPath());
            EmailSender.sendMail(user.getEmail(), "Kľúč do Jenkins",
                    "Ahoj " + user.getName() + ",\n\n" +
                            "nasledujúci kľúč \n" +
                            secretKey.substring(0, secretKey.length() - 1) +
                            "\n\n potrebuješ použiť na linke:\n\n" +
                            jenkins.getUrlForJenkins() +
                            "\n\n\n MENO: admin" +
                            "\nHESLO: 0000 (toto heslo vám odporúčáme zmeniť)" +
                            "\n\n Prosím nemente práva používateľa reg_test ak chcete naďalej využívať výhody našej aplikáie");
            jenkins.setLastSecurityEmail(new Date(System.currentTimeMillis()));
            jenkinsRepository.save(jenkins);
            return true;//"Running";
        }
        return false;//"It is not your Jenkins!!!";
    }



    private boolean isJenkinsOnline(Jenkins jenkins) {
        int i = 0;
        while (!jenkins.isRuning()) {
            try {
                if (i > 50)
                    return false;
                i++;
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    private void changeJenkinsStatus(Jenkins j, String status, String classs){
        j.setStatus(status);
        j.setStatusClasses(classs);
        jenkinsRepository.save(j);
    }

    private JobWithDetails getMasterJobByName(String jobName){
        JenkinsServer jenkinsServer;
        try {
            jenkinsServer = JenkinsGen.getInstance("8079", "", "duri", "14721993").getJenkinsServer();
            return jenkinsServer.getJob(jobName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getSecretKey(String path) {
        String secretFile = Constants.getAbsolutePath() + "/jenkinses/" + path + "/secrets/initialAdminPassword";
        try {
            return readFile(secretFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "notExists";
    }

    private String readFile(String fileName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            return sb.toString();
        } finally {
            br.close();
        }
    }

}
