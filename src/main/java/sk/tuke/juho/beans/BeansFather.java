package sk.tuke.juho.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import sk.tuke.juho.controllers.entities.Jenkins.JenkinsRepository;
import sk.tuke.juho.controllers.entities.account.AccountRepository;
import sk.tuke.juho.controllers.entities.project.Project;
import sk.tuke.juho.controllers.entities.project.ProjectRepository;
import sk.tuke.juho.controllers.entities.team.Team;
import sk.tuke.juho.controllers.entities.team.TeamRepository;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Juraj Horbal on 2/8/2017.
 * Parents Bean, that contains all the same functions of its children
 * (Rodičovský Bean - zahŕňa všetky spolo\u010Dné funcie potomkov)
 */
public class BeansFather {
    @Autowired
    protected TeamRepository teamRepository;
    @Autowired
    protected AccountRepository accountRepository;
    @Inject
    @Autowired
    protected ProjectRepository projectRepository;

    @Autowired
    protected JenkinsRepository jenkinsRepository;

    /**
     * constructor (konštruktor)
     */
    BeansFather() {
    }

    /**
     * Find id of user by his email (zisťuje ID používateľa podľa jeho emailu)
     * @return id of user (ID hľadaného používateľa)
     */
    public long getActualId() {
        return accountRepository.findOneByEmail(getUserEmail()).getIdAccount();
    }

    /**
     * Get email of current user (Získanie emailu aktuálneho používateľa)
     * @return email of current user (email aktuálneho používateľa)
     */
    public String getUserEmail() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            return ((User) authentication.getPrincipal()).getUsername();
        }
        return "";
    }


    /**
     * Hladá zoznam projektov podľa toho, v ktorých tímoch je aktuálny používateľ členom
     * @return zoznam projektov aktuálneho používateľa
     */
    public List<Project> getProjects() {
        return projectRepository.findByTeamIn(getTeams());
    }

    /**
     * Hladá zoznam tímov aktuálneho používateľa
     * @return zoznam tímov aktuálneho používateľa
     */
    public List<Team> getTeams() {
        String email = getUserEmail();
        if (email.equals(""))
            return new ArrayList<>();
        List<Team> teams = teamRepository.findByAccounts(accountRepository.findOneByEmail(email));
        return teams;
    }
}
