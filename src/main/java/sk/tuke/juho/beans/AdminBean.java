package sk.tuke.juho.beans;

/**
 * Created by Juraj on 27.12.2016.
 */

import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import sk.tuke.juho.controllers.entities.account.Account;
import sk.tuke.juho.controllers.entities.project.Project;
import sk.tuke.juho.controllers.entities.team.Team;

import javax.faces.bean.ManagedBean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * zabezpečuje operácie pre administrátorské konto - zoznam všetkých tímov a projektov s prislúchajúcimi používateľmi
 */

@Component
@ManagedBean(name = "adminBean")
//@RequestScoped
@Scope("session")
//@ViewScoped
public class AdminBean extends BeansFather implements Serializable {

    /**
     * Return list of all teams for Administrator of application (Administrátorovi celej aplikácie vráti zoznam všetkých tímov)
     * @return list of all teams (zoznam všetkých dostupných tímov)
     */
    public List<Team> getTeams() {
        if (isAdmin())
            return teamRepository.findAll();
        return new ArrayList<>();
    }

    /**
     * Return list of all teams for Administrator of application (Administrátorovi celej aplikácie vráti zoznam všetkých projektov)
     * @return list of all projects (zoznam všetkých dostupných projektov)
     */
    public List<Project> getProjects() {
        if (isAdmin())
            return projectRepository.findAll();
        return new ArrayList<>();
    }

    /**
     * Control if current user is the administrator of application (Kontrola či aktuálny používateľ je administrátor celej aplikácie)
     * @return true if user is the administrator of application (ak používateľ je administrátor celej aplikácie)
     */
    public boolean isAdmin() {
        String userEmail = getUserEmail();
        if (userEmail.equals(""))
            return false;
        Account account = accountRepository.findOneByEmail(userEmail);
        return account.hasRole("ROLE_ADMIN");
    }

    public String getUserEmail() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || authentication.getPrincipal().equals("anonymousUser"))
            return "";
        return ((User) authentication.getPrincipal()).getUsername();
    }
}
