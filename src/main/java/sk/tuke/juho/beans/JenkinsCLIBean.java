package sk.tuke.juho.beans;

import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.model.JobWithDetails;
import com.offbytwo.jenkins.model.QueueReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import sk.tuke.juho.controllers.entities.Jenkins.JenkinsCliRepository;
import sk.tuke.juho.controllers.entities.account.Account;
import sk.tuke.juho.controllers.entities.team.Team;
import sk.tuke.juho.utils.Constants;
import sk.tuke.juho.utils.JenkinsGen;

import javax.faces.bean.ManagedBean;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

/**
 * Created by Juraj Horbaľ on 27.12.2016. poskytuje podporu pre Jenkins Cli príkazy
 */

@Component
@ManagedBean(name = "jenkinsCliBean")
//@RequestScoped
@Scope("session")
//@ViewScoped
public class JenkinsCLIBean extends BeansFather implements Serializable {

    @Autowired
    protected JenkinsCliRepository jenkinsCliRepository;

    private String command = "install-plugin";

    private String parameter = "";
    private String user = "";
    private String password = "";
    private static List<String> commands;
    static{
        commands = new LinkedList<String>();
         //label, value
        commands.add("reload-configuration");
        commands.add("disable-job");
        commands.add("enable-job");
        commands.add("install-plugin");
    }

    /**
     *
     * @return List of possible commands for CLI
     */
    public List<String> getCommands() {
        return commands;
    }

    /**
     *
     * @return current command
     */
    public String getCommand() {
        return command;
    }

    /**
     *
     * @param command current CLI commmand
     */
    public void setCommand(String command) {
        this.command = command;
    }

    /**
     *
     * @return current password of Jenkins user for cli command
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password current password of Jenkins user for cli command
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @return current username of Jenkins user for cli command
     */
    public String getUser() {
        return user;
    }

    /**
     *
     * @param user current username of Jenkins user for cli command
     */
    public void setUser(String user) {
        this.user = user;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    /**
     * constructor
     */
    public JenkinsCLIBean() {
    }

    /**
     * It find Jenkins job on head Jenkins, set all parameters and call build with parameters.
     * @param id of the team
     * @return true if command was executed
     */
    public boolean executeCliCommand(Long id){
        Team team = teamRepository.findOne(id);
        Account user = accountRepository.findOneByEmail(getUserEmail());

        if (team.teamContainsUser(user)) {
            JobWithDetails jwd = getMasterJobByName("reload configuration");
            if(jwd == null)
                return false;

            String absolutePath = Constants.getAbsolutePath();
            String javaHome = absolutePath + "\\jenkinses\\Jenkins\\jre\\bin";

            checkParameter();

            Map<String, String> params = new HashMap<>();
            params.put("java_home_j", javaHome);
            params.put("jenkins_cli_home", absolutePath);
            params.put("jenkins_url_with_port_j", team.getJenkins().getUrlForJenkins());
            params.put("cli_command", command+" "+ parameter);
            params.put("user", Constants.jenkinsUserName);
            params.put("passw", Constants.jenkinsUserPassowrd);

            try {
                QueueReference queueReference = jwd.build(params);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return false;
    }

    private void checkParameter() {
        if(parameter.contains(" & ") || parameter.contains(" && ") || parameter.contains(" | ") || parameter.contains(" || "))
            parameter="";
    }

    /**
     * It find Jenkins job on head Jenkins, set all parameters and call build with parameters.
     * @param id of the team
     * @return true if command was executed
     */
    public boolean reloadJenkins(Long id) {
        Team team = teamRepository.findOne(id);
        Account user = accountRepository.findOneByEmail(getUserEmail());

        if(team.isAdmin(user)){
            JobWithDetails jwd = getMasterJobByName("reload configuration");
            if(jwd == null)
                return false;

            String absolutePath = Constants.getAbsolutePath();
            String javaHome = absolutePath + "\\jenkinses\\Jenkins\\jre\\bin";

            Map<String, String> params = new HashMap<>();
            params.put("java_home_j", javaHome);
            params.put("jenkins_cli_home", absolutePath);
            params.put("jenkins_url_with_port_j", team.getJenkins().getUrlForJenkins());
            params.put("user", Constants.jenkinsUserName);
            params.put("passw", Constants.jenkinsUserPassowrd);

            try {
                jwd.build(params);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }


    private JobWithDetails getMasterJobByName(String jobName){
        JenkinsServer jenkinsServer;
        try {
            jenkinsServer = JenkinsGen.getInstance("8079", "", "duri", "14721993").getJenkinsServer();
            return jenkinsServer.getJob(jobName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
