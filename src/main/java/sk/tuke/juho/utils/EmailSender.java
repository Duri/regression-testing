package sk.tuke.juho.utils;

/**
 * Created by Juraj Horbaľ on 1/17/2017. Zabezpečuje posielanie Emailov
 */

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

public class EmailSender {

    /**
     *
     * @param email email address
     * @param emailSubject subject of email
     * @param emailText content of email
     */
    public static void sendMail(String email, String emailSubject, String emailText) {

        emailText = emailText + "\n\n" +
                "S pozdravom,\n" +
                "Tím Regresné testovanie ako služba"
                + "\n\n" + "This is email for: " + email;

        final String username = "regresne.testovanie@gmail.com";
        final String password = "MocneH3slo6reD1pl0mku";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.mime.charset", "UTF-8");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));//
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse("juraj.horbal@gmail.com"));//"to-email@gmail.com"
//            message.setText("Dear Mail Crawler,"
//                    + "\n\n No spam to my email, please!");
            try {
                message.setSubject(MimeUtility.encodeText(emailSubject, "UTF-8", "B"));//"Testing Subject"
                message.setText(emailText);
//                message.setContent(emailText, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}
