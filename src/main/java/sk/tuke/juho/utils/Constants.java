package sk.tuke.juho.utils;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;

/**
 * Created by Juraj Horbaľ on 15.8.2016. Konstanty a pomocné metódy pre zistenie statických informácii o aplikácii
 */
public class Constants {
    //    public static final String GENERATE = "C:\\Generate\\";
//    public static final String DOCKER_HOME = "C:\\Program Files\\Docker Toolbox\\";
    public static final int RESEND_EMAIL_TIMEOUT_IN_MILISECS = 20 * 60 * 1000;
    public static final String jenkinsUserName = "reg_test";
    public static final String jenkinsUserPassowrd = "Very5tr0ngpAs5worD";
    private static String hostName;
    private static String hostIP;
    private static String absolutePath;

//    public static String getMainDirForUser(Account account) {
//        String path = GENERATE + account.getEmail().substring(0,
//                account.getEmail().indexOf("@")) + "_" + account.getIdAccount() + "\\";
//        File file = new File(path + "Dockerfile");
//        if (!file.getParentFile().getParentFile().exists())
//            file.getParentFile().getParentFile().mkdir();
//        if (!file.getParentFile().exists())
//            file.getParentFile().mkdir();
//        return path;
//    }

    /**
     *
     * @return local host name
     */
    public static String getLocalHostName() {
        if (hostName == null) {
            try {
                hostName = InetAddress.getLocalHost().getHostName();
            } catch (UnknownHostException e) {
                e.printStackTrace();
                throw new RuntimeException("");
            }
        }
        return hostName;
    }

    /**
     *
     * @return local host IP
     */
    public static String getLocalHostIP() {
        if (hostIP == null) {
            try {
                hostIP = InetAddress.getLocalHost().getHostAddress();
            } catch (UnknownHostException e) {
                e.printStackTrace();
                throw new RuntimeException("");
            }
        }
        return hostIP;
    }

    /**
     *
     * @return absolute path to the working directory
     */
    public static String getAbsolutePath() {
        if (absolutePath == null) {
            File f = new File("f.txt");
            absolutePath = f.getAbsolutePath().substring(0, f.getAbsolutePath().lastIndexOf("\\"));
            f.delete();
        }
        return absolutePath;
    }

    /**
     *
     * @param url_page url of tested page
     * @return true if request doesn't return response code 404
     */
    public static boolean isPageOnline(String url_page) {
        URL url = null;
        try {
            url = new URL(url_page);

            HttpURLConnection huc = null;

            huc = (HttpURLConnection) url.openConnection();
            huc.setRequestMethod("HEAD");
            huc.setConnectTimeout(100);
            int responseCode = huc.getResponseCode();

            return responseCode != 404;

        } catch (IOException e) {
            //Jenkins is not running
//            System.out.println(e.getMessage());
        }
        return false;
    }
}
