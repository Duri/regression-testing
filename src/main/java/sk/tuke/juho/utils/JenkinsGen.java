package sk.tuke.juho.utils;

import com.offbytwo.jenkins.JenkinsServer;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by Juraj Horbaľ on 15.8.2016. Prepojenie s Jenkins Inštnciami
 */
public class JenkinsGen {
    private JenkinsServer jenkinsServer;
    private static JenkinsGen jenkinsGen;

    private JenkinsGen() {
        this("8079", "", "duri", "14721993");
    }

    private JenkinsGen(String port, String path, String user, String pswOrKey) {
        String hostName = "";
        hostName = Constants.getLocalHostName();
        String url = "http://" + hostName + ":" + port + path;
        try {
            if (user == null && pswOrKey == null)
                jenkinsServer = new JenkinsServer(new URI(url));
            else
                jenkinsServer = new JenkinsServer(new URI(url), user, pswOrKey);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    /**
     * Singleton for jenkins main instance
     * @return main Jenkins instance
     */
    public static JenkinsGen getInstance() {
//        if (jenkinsGen == null)
        jenkinsGen = new JenkinsGen();
        return jenkinsGen;
    }

    /**
     * Singleton for jenkins main instance
     * @param port port of instance
     * @param path path to the instance
     * @param user user name of user that can make operations
     * @param pswOrKey password or access key of user
     * @return main Jenkins instance
     */
    public static JenkinsGen getInstance(String port, String path, String user, String pswOrKey) {
//        if(jenkinsGen==null)
        jenkinsGen = new JenkinsGen(port, path, user, pswOrKey);
        return jenkinsGen;
    }

    public static JenkinsGen getInstance(String port, String path) {
//        if(jenkinsGen==null)
        jenkinsGen = new JenkinsGen(port, path, null, null);
        return jenkinsGen;
    }

    /**
     *
     * @return current used Jenkins instance
     */
    public JenkinsServer getJenkinsServer() {
        return jenkinsServer;
    }
}
