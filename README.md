#Regression testing as a service

Regression testing of the software serves to verify that the program's functionality is maintained.
******
####Description:
* use the available Jenkins options
+ main Jenkins instance allows:
    * downloading from repository
    * building
    * testing
    * deployment of a web application
    * **manages individual Jenkins user instances**
* Web application allows users to automatically generate and manage **their own Jenkins instance**.   `

#####For running you need:
   1.  create directory `C:\jenkins`
   2.  copy config.xml from `\jenkinses\Jenkins\home\` to your `C:\jenkins`
   3.  copy jobs from `\jenkinses\Jenkins\home\jobs` to your `C:\jenkins\jobs` directory
   4.  copy plugins from `\jenkinses\Jenkins\home\plugins` to your `C:\jenkins\plugins` directory
   5.  copy admin user from `\jenkinses\Jenkins\home\users\admin` to your `C:\jenkins\users` directory (password is `0000`)
   6.  Start Command line from main directory of repository (`regression-testing`) run command `Jenkinses\Jenkins\jre\bin\java -DJENKINS_HOME=C:\jenkins -jar jenkinses\usr\jenkins.war --httpPort=8079`(Jenkins version is 2.32.3)
   7.  Reload plugins on  [http://localhost:8079/pluginManager](http://localhost:8079/pluginManager/) and restart Jenkins instance.
   8.  Start job `build_app`
   9.  Web application should be started on [http://localhost:800](http://localhost:8080)


###Used technologies:
![Scheme](imgs/technologies.png)
* * *
#####Resources

1. [Markdown Demo for Bitbucket](https://bitbucket.org/tutorials/markdowndemo)
2. [Jenkins](https://jenkins.io/)
3. [Maven](https://maven.apache.org/)

Created by [Juraj](https://www.linkedin.com/in/juraj-kicko-horba%C4%BE-404192130/)
